package utility;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class LaunchBrowser {
	
	

	@BeforeTest
	public static void browserOpen(){
	{
	System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");	
	WebDriver driver = new ChromeDriver();
	driver.get("http://qa.edgars.co.za/");
	driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
	driver.manage().window().maximize();
	String expected = "http://qa.edgars.co.za/";
	String actual = driver.getCurrentUrl();
	try{
	if(expected.equals(actual)){
		System.out.println(actual);
		}
	}
	catch(Exception e){
	System.out.println(e.getMessage());	
	}
	}	
}
}
	