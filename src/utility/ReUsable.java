package utility;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ReUsable {
	
	public static boolean refreshPage(String pageTitle, WebDriver driver, int refreshCount){
		driver.getTitle();
		for(int i=0; i<refreshCount; i++){
			if(!pageTitle.equals(driver.getTitle())){
				driver.navigate().refresh();
				 driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			}
			else{
				return true;
			}
		}
		
		throw new AssertionError("Coludn't find the page with title " + pageTitle);	
		
	}

	}


