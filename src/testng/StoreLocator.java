package testng;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class StoreLocator {

	public static WebDriver driver;
	static String names, names1,names2,names3;

	@BeforeTest
	public static void logoutUserStoreLocator() {

		System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");
		 driver = new ChromeDriver();
		//driver = new FirefoxDriver();
		driver.get("http://qa.edgars.co.za/");
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	}

	@Test(priority = 1)
	public static void logOutUStoreLocator() throws InterruptedException {
		driver.findElement(By.xpath("//a[@title='Store Locator']")).click();
         WebElement li = driver.findElement(By.xpath("//select[@id='province-select']"));
		Select dropdown = new Select(li);
		List<WebElement> all = dropdown.getOptions();
		int size = all.size();
		for (int i = 1; i < size; i++) {
			WebElement index = all.get(i);
			names = index.getText();
			System.out.println("dropdown list names are" + names);
		}
		List<WebElement> e;
		try {
			e = driver.findElements(By.xpath("//div[@id='content']/ul/li/a"));
			int size1 = e.size();
			for (int i = 0; i < size1; i++) {
				WebElement index1 = e.get(i);
				names1 = index1.getText();
				System.out.println("Select a Province names are " + names1);

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			System.out.println(e1.getMessage());
		}
		Thread.sleep(2000);
		if (names.matches(names1)) {
			System.out.println("Drop down list provinces and 'Select a Province' are same names");
		}
	}
}
	
	/*@Test(priority = 2)
	public static void logOutUFindAStore() throws InterruptedException {
		driver.findElement(By.xpath("//a[text()='Find a store']")).click();
		boolean b= driver.findElement(By.xpath(".//*[@id='storelocator-container']")).isDisplayed();
		System.out.println("find store is dispalying" + b);
		driver.switchTo().window("storelocator-container");
	try{
		String s = driver.findElement(By.xpath("//*[@id='content']/ul/li/span")).getText();
        System.out.println(s);
	}
	catch(Exception e){
		 System.out.println(e.getMessage());
	}
	}
		WebElement li1 = driver.findElement(By.id("province-select"));
		Select dropdown1 = new Select(li1);
		List<WebElement> all1 = dropdown1.getOptions();
		int size2 = all1.size();
		for (int i = 1; i < size2; i++) {
			WebElement index1 = all1.get(i);
			names2 = index1.getText();
			System.out.println("dropdown list names are" + names2);
		}
		List<WebElement> e2;
		try {
			e2 = driver.findElements(By.xpath("//div[@id='content']/ul/li/a"));
			int size3 = e2.size();
			for (int i = 0; i < size3; i++) {
				WebElement index2 = e2.get(i);
				names3 = index2.getText();
				System.out.println("Select a Province names are " + names3);

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		Thread.sleep(2000);
		if (names2.matches(names3)) {
			System.out.println("Drop down list provinces and 'Select a Province' are same names");
		}
	}
	
	

}*/
