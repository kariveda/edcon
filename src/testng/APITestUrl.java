package testng;


import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class APITestUrl {
	
        ArrayList<String> linkcount;
		@Test(priority=0)
		public void basicTest() throws Exception {
			// TODO Auto-generated method stub
			System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			linkcount = new ArrayList();
			driver.get("http://qa.edgars.co.za");
			driver.manage().window().maximize();
		    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		    List<WebElement> alllinks = driver.findElements(By.tagName("a"));
		    for(WebElement ele:alllinks ){
		    	String linkssrc =ele.getAttribute("href");
		    	verifyHomeBrokenLinks(linkssrc);
		    }
		    System.out.println("Total boken links are" + linkcount.size());
		    driver.quit();
		}
		
		public void verifyHomeBrokenLinks(String url){
	             int code=0;
			try {
				URL link = new URL(url);
				HttpURLConnection conn = (HttpURLConnection)link.openConnection();
				conn.connect();
				conn.setConnectTimeout(20000);
				code = conn.getResponseCode();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Link is getting tested " +url+ "and response code is" +code);
			if(code==200){
				System.out.println("Link is verified");
				}
			else
			{
				linkcount.add(url);
				System.out.println("Bad link");	
			}

	}

	}

