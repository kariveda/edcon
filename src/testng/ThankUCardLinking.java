package testng;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.commons.io.FileUtils;
import org.apache.james.mime4j.message.Message;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ThankUCardLinking {
  
	public static  WebDriver driver;
	public String expected = null;
	public String actual = null;
	
	
    public static String baseUrl = "http://qa.edgars.co.za";
 

    @BeforeTest
    public static void launchBrowser(){
	System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");
	driver = new ChromeDriver();
    //driver = new FirefoxDriver();
	driver.get(baseUrl);
	String expected = "Edgars | latest fashion for women, men�s and kids, beauty & homeware";
	String actual = driver.getTitle();
	Assert.assertEquals(actual, expected);
	System.out.println(actual);
	driver.manage().window().maximize();
}
@Test(priority=0)
public static void logIn() throws InterruptedException{
driver.findElement(By.linkText("LOG IN")).click();
String expected = "Customer Login";
String actual = driver.getTitle();
Thread.sleep(3000);
Assert.assertEquals(actual, expected);
System.out.println(actual);
}

@Test(priority=1)
public static void captureDeatils() throws InterruptedException{
driver.findElement(By.id("email")).sendKeys("anusha1223kariveda@gmail.com");
driver.findElement(By.id("pass")).sendKeys("edgars");
driver.findElement(By.id("send2")).click();
driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
String expected = "My Account";
String actual = driver.getTitle();
Thread.sleep(3000);
Assert.assertEquals(actual, expected);
System.out.println(actual);
driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[2]/div[2]/div/div[3]/div/div[2]/ul/li[7]/a")).click();  //clicking on thank you card link under my account
}  

public static void thankYouCardsLinks() throws InterruptedException, IOException{
	WebElement linkElement=null;
	try{
		linkElement = driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div/div[2]/p/a"));  
	   }
	catch(Exception ex){
		System.out.println(ex.getMessage());
	   }
	//driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	if(linkElement!=null && linkElement.isDisplayed())
	{
	linkElement.click();
	}
	 else{
		 System.out.println("There were no card linked here");
	}
}
public static void popUpWindow(){
	boolean isnegativeee = false; 
			try{
				if(driver.findElement(By.xpath("./html/body/div[3]/div[2]/div[2]/ul/li")) != null){
					isnegativeee = driver.findElement(By.xpath("./html/body/div[3]/div[2]/div[2]/ul/li")).isDisplayed();
				}
				}
			catch(Exception e){
				
			}
	if(isnegativeee)
	{
  		System.out.println("xpath------------------");

		driver.findElement(By.xpath(".//html/body/div[3]/div[1]")).click();
	}
	else if(driver.findElement(By.id("otp")).isDisplayed())
	{
  		System.out.println("id --------------------------");

		 WebElement linkElement1= null;
		 String currentWindow = driver.getWindowHandle();
	  	   	try{
	  	   		linkElement1 = driver.findElement(By.id("otp"));  
	  	   	 }
	  	   	catch(Exception ex){
	  	   		System.out.println(ex.getMessage());
	  	   	   }
	  	       if(linkElement1.isDisplayed())  //pop window handle for otp pin
	  	       {
	  			 ThankUCardLinking.backEndLogin(currentWindow);
	  			 
	         	}
	    	else
	    	{
	  		System.out.println("There is no `verify account` pop up window");
	  	    }		
	}else{
  		System.out.println("There is no `verify account` pop up window");

	}
}

public static void waitForPopUpWindow() throws InterruptedException{
	WebElement linkElement=driver.findElement(By.xpath(".//html/body/div[3]/div[2]"));
	if(linkElement.isDisplayed())
	{
		int waitingTime =0;
		while(linkElement.isDisplayed() && (waitingTime < 60)){
			if(!linkElement.getText().contains("Sending request")){
				return;
			}
			Thread.sleep(2000);
			waitingTime++;
		}
	}
	
}
public static void clearValues(){
	driver.findElement(By.id("edcon_card_number")).sendKeys(Keys.RETURN);     
	  try{
		  driver.findElement(By.id("edcon_card_number")).clear();             //Clear values in thank you card number text
	 }catch(Exception e){
		 System.out.println("Couldn't clear the values");
	 }
	driver.findElement(By.id("mobile_number")).sendKeys(Keys.RETURN);
	   try{
			driver.findElement(By.id("mobile_number")).clear();  //clear values in cell number text feild
	  }catch(Exception e){
		System.out.println("Couldn't clear the values");
	  }
}

public static void backEndLogin(String currentWindow){
	driver.switchTo().frame(currentWindow); // switch back
	driver.findElement(By.cssSelector("Body")).sendKeys(Keys.CONTROL + "t");
	driver.get("http://qa.edgars.co.za/admin");
	driver.findElement(By.id("username")).sendKeys("anusha");
	driver.findElement(By.id("login")).sendKeys("edgars1#");
	driver.findElement(By.xpath(".//html/body/div/div/form/div/div[5]/input")).click();
	driver.manage().timeouts().implicitlyWait(1,TimeUnit.MINUTES);
	if(driver.findElement(By.id("message-popup-window")).isDisplayed())
	{
	driver.findElement(By.xpath(".//html/body/div[1]/div[4]/div[1]/a/span")).click();
	}
	else
	{
		System.out.println("failed to click on close button or pop up not avialble");
	}
	String expected = "Dashboard / Magento Admin";
	String actual = driver.getTitle();
	Assert.assertEquals(actual, expected);
	System.out.println(actual);
	if(actual.equals(expected)){
		driver.findElement(By.xpath(".//html/body/div[1]/div[1]/div[3]/ul/li[11]/a/span")).click();  //clicking on Fontera tab 
		driver.findElement(By.xpath(".//html/body/div[1]/div[1]/div[3]/ul/li[11]/ul/li[2]/a/span")).click();  //clicking on sms status list
	}
	else{
		System.out.println("Failed to open sms status list");
	}
		String expected1 = "SMS Status List / Magento Admin";
		String actual1 = driver.getTitle();
		Assert.assertEquals(actual1, expected1);
		System.out.println(actual1);
		if(actual1.equals(expected1)){
			driver.navigate().refresh();
			driver.findElement(By.xpath(".//html/body/div[1]/div[4]/div/div[3]/div/div[2]/div/table/tbody/tr[1]/td[4]/a")).click();  //clicking on 1st view link for OTP
			String message = driver.findElement(By.xpath(".//html/body/div[1]/div[4]/div/div[3]/div/div[2]/div/table/tbody/tr[1]/td[4]/div/pre")).getText();
		    System.out.println(message);
		    message = message.replace("You have requested to link your Thank U card at Edgars. Your One Time Pin is: "," ");
	       System.out.println(message);
	       
		   driver.findElement(By.id("otp")).sendKeys(message);  //Edgars front end on verify account pop up window
	       driver.findElement(By.xpath(".//html/body/div[3]/div[2]/div[2]/div[2]/form/div[2]/button[2]")).click();  //clicking on submit button
		     }
		else
		{
			System.out.println("failed to click on the link");
		}
		/*WebElement
       if(){   //after submiting otp handling error messages
    	  System.out.println(); 
       }
       else{
    	   System.out.println("Oredr placed suceessfully"); 
       }
	     */ 
}
/*public static void successCard(){
	WebElement linkElement1 = null;
	   	try{
	   		linkElement1 = driver.findElement(By.xpath(".//html/body/div[3]/div[2]"));  
	   	   }
	   	catch(Exception ex){
	   		System.out.println(ex.getMessage());
	   	   }
	       if(linkElement1.isDisplayed() && linkElement1!=null)  //pop window handle for otp pin
	{
			 ThankUCardLinking.backEndLogin(currentWindow);
	}
	else
	{
		System.out.println("There is no `verify account` pop up window");
	}	
	}*/

@Test(priority= 2,dataProvider = "testdata")
	 public static void thankYouCardInfo(String thankyoucardnumber,String mobilenumber) throws IOException, InterruptedException
		{
		 ThankUCardLinking.thankYouCardsLinks();
		driver.findElement(By.id("edcon_card_number")).sendKeys(thankyoucardnumber);  //thank you cardnumber
		driver.findElement(By.id("mobile_number")).sendKeys(mobilenumber);            //mobile number
		driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div/form/div[2]/div[3]/button")).click();  //Clicking on submit button
		//driver.manage().timeouts().implicitlyWait(1,TimeUnit.MINUTES);
		//String currentWindow = driver.getWindowHandle();
		ThankUCardLinking.waitForPopUpWindow();
		//screen shot capturing
		 TakesScreenshot ts1 =(TakesScreenshot)driver;
		 File source = ts1.getScreenshotAs(OutputType.FILE);
		 driver.findElement(By.name("edcon_card_number"));
		 String screenShotFName = "./ThankucardLinks/" + thankyoucardnumber + "/valid.png";
		 FileUtils.copyFile(source, new File(screenShotFName));
		 System.out.println("screen shot taken");
		 //screen shot capturing end
	
        ThankUCardLinking.popUpWindow();
        ThankUCardLinking.clearValues();
         
  	  }
		 
	    @DataProvider(name = "testdata")
		public Object[][] readExcel() throws BiffException, IOException {
			// TODO Auto-generated method stub
			File f = new File("C:/Users/InventUser/Desktop/datadriven3.xls");
			Workbook w = Workbook.getWorkbook(f);
			Sheet s = w.getSheet("ThankyouCard");
			int rows = s.getRows();
		    int columns = s.getColumns();
		    //System.out.println(rows);
		    //System.out.println(columns);
		    String results = "";
		    boolean done = false;
		    String inputData[][] = new String [rows][columns];
	           for(int i=0; i<rows && !done; i++) {
		    	for(int j=0; j<columns && !done; j++){
		    		Cell c = s.getCell(j,i);
		    		results  =  (String)c.getContents();
		    		if(results==null || results.isEmpty() || results.length()<=0){
		    			done = true;
		    		}
		    		else{
		    			inputData[i][j] = c.getContents();
			    		System.out.println(inputData[i][j]);
		    		}
		    	}
	           }
		   return inputData; 
		}
		
	}
	



