package testng;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class WelcomeMessageValidation {

	  public static WebDriver driver;
	 @Test(priority= 1,dataProvider = "testdata")
		public void validLogins(String username,String password) throws InterruptedException
		{
		System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");	
		driver = new ChromeDriver();
		//driver = new FirefoxDriver();
		driver.get("http://qa.edgars.co.za/");
		driver.manage().window().maximize();
		driver.findElement(By.linkText("LOG IN")).click();
		driver.findElement(By.name("login[username]")).sendKeys(username);
	    driver.findElement(By.name("login[password]")).sendKeys(password);
		driver.findElement(By.id("send2")).click();
		driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
		String user =null;
		WelcomeMessageValidation.welcomeMsgVerify(user);
		}
		
		@AfterMethod  
		public void closeBrowser(){
			driver.close();
		}

	    @DataProvider(name = "testdata")
		public Object[][] readExcel() throws BiffException, IOException {
			// TODO Auto-generated method stub
			File f = new File("C:/Users/InventUser/Desktop/datadriven3.xls");
			Workbook w = Workbook.getWorkbook(f);
			Sheet s = w.getSheet("Logins");
			int rows = s.getRows();
		    int columns = s.getColumns();
		    //System.out.println(rows);
		    //System.out.println(columns);
		    
		    String inputData[][] = new String [rows][columns];
	           for(int i=0; i<rows; i++) {
		    	for(int j=0; j<columns; j++){
		    		Cell c = s.getCell(j,i);
		    		inputData[i][j] = c.getContents();
		    		System.out.println(inputData[i][j]);
		    		}
		    	}
		   return inputData; 
		}

	
	public static void welcomeMsgVerify(String user) throws InterruptedException{
		
	String expected = "My Account";
	String actual= driver.getTitle();
    Assert.assertEquals(actual, expected);
    if(actual.equals(expected)){
    System.out.println(actual);
    driver.findElement(By.xpath("//a[@href='http://qa.edgars.co.za/customer/account/edit/']")).click();
    String expected1= "Account Information";
    String actual1= driver.getTitle();
    Assert.assertEquals(actual1, expected1);
    
    if(actual1.equals(expected1)){
     user = driver.findElement(By.id("firstname")).getAttribute("value");
    System.out.println(user);
    }
    else{
    System.out.println("Unable to capture username");
    	 }
       }
	String msg = driver.findElement(By.xpath("//p[@class='welcome-msg']")).getText();
	System.out.println(msg);
	msg = msg.replace("WELCOME, "," ");
	 System.out.println(msg);
	  msg = msg.replaceAll("[!]","");
	  String result = msg.toLowerCase();
     System.out.println(result);
     System.out.println("Login user "+user);
     Thread.sleep(1000);
	}
}
	


