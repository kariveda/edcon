package testng;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

@Test
public class LoginRequired {
	
	
	 public WebDriver driver;
	 
	 public void login() throws IOException, InterruptedException
	{   
      System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");
	  WebDriver driver = new ChromeDriver();  
		 //WebDriver driver = new FirefoxDriver();
	  driver.get("http://qa.edgars.co.za");
	  driver.manage().window().maximize();
	  driver.findElement(By.linkText("LOG IN")).click();  
	  driver.findElement(By.name("login[username]")).sendKeys();
	  driver.findElement(By.name("login[password]")).sendKeys();
	  driver.findElement(By.id("send2")).click();
	
	 String errormessage1 = driver.findElement(By.id("advice-required-entry-email")).getText();     //error message capturing
	 String errormessage2 = driver.findElement(By.id("advice-required-entry-pass")).getText();
	 System.out.println(errormessage1);
	 System.out.println(errormessage2);
	 Thread.sleep(2000);
	 
	 //screen shot capturing start
	 TakesScreenshot ts =(TakesScreenshot)driver;
	 File source= ts.getScreenshotAs(OutputType.FILE);
	 FileUtils.copyFile(source, new File("./Loginrequired/Requiredfeilds.png"));
	 System.out.println("screen shot taken");
	 //screen shot capturing end
	 
	 Thread.sleep(2000);
	  driver.findElement(By.name("login[username]")).sendKeys("qatesting.commerce@gmail.com");
	 driver.findElement(By.name("login[password]")).sendKeys();
	  driver.findElement(By.id("send2")).click();
	 String errormessage3 = driver.findElement(By.id("advice-required-entry-pass")).getText();
	 System.out.println(errormessage3);
	 Thread.sleep(2000);
	 
	 //screen shot capturing
	 TakesScreenshot ts1 =(TakesScreenshot)driver;
	 File source1 = ts1.getScreenshotAs(OutputType.FILE);
	 FileUtils.copyFile(source1, new File("./Loginrequired/Passwordrequired.png"));
	 System.out.println("screen shot taken");
	//screen shot capturing end
	
	 
	driver.findElement(By.id("email")).sendKeys(Keys.RETURN);
	  try{
		  driver.findElement(By.id("email")).clear();
	 }catch(Exception e){
		 System.out.println("Couldn't clear the values");
	 }
	  
	 Thread.sleep(2000);
	 driver.findElement(By.name("login[username]")).sendKeys();
	 driver.findElement(By.name("login[password]")).sendKeys("edgars");
	  driver.findElement(By.id("send2")).click();
	  Thread.sleep(2000);
	 String errormessage4 = driver.findElement(By.id("advice-required-entry-email")).getText();
	 System.out.println(errormessage4);
	 Thread.sleep(2000);
	 
	 //screen shot capturing
	 TakesScreenshot ts2 =(TakesScreenshot)driver;
	 File source2= ts2.getScreenshotAs(OutputType.FILE);
	 FileUtils.copyFile(source2, new File("./Loginrequired/Emailrequired.png"));
	 Thread.sleep(2000);
	 System.out.println("screen shot taken");
	//screen shot capturing end
	 

	driver.findElement(By.id("pass")).sendKeys(Keys.RETURN);
	  try{
		driver.findElement(By.id("pass")).clear();
		 }catch(Exception e){
		System.out.println("Couldn't clear the values");
		 }
	}
	
}

	
 
 
 
		