package testng;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class ThankYouCradLinks {
	
	public static  WebDriver driver;
	public String expected = null;
	public String actual = null;
	public static String baseUrl = "http://qa.edgars.co.za";
	
	 @BeforeTest
	    public static void launchBrowser(){
		//System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");
		//driver = new ChromeDriver();
		 driver = new FirefoxDriver();
		driver.get(baseUrl);
		String expected = "Edgars | latest fashion for women, men�s and kids, beauty & homeware";
		String actual = driver.getTitle();
		Assert.assertEquals(actual, expected);
		System.out.println(actual);
		driver.manage().window().maximize();
	}
	 
	 
	 
	 @Test(priority = 1)
	 public static void captureDeatils() throws InterruptedException{
		 driver.findElement(By.linkText("LOG IN")).click(); 
	 driver.findElement(By.id("email")).sendKeys("qatesting.commerce@gmail.com");
	 driver.findElement(By.name("login[password]")).sendKeys("edgars#");
	 driver.findElement(By.id("send2")).click();
	 String expected = "My Account";
	 String actual = driver.getTitle();
	 Thread.sleep(3000);
	 Assert.assertEquals(actual, expected);
	 System.out.println(actual);
	 }
    
    @Test(priority=2)
    public static void unlinkThankYouCrad(){  //unlink thank you card if it is linked already
    	driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[2]/div[2]/div/div[3]/div/div[2]/ul/li[7]/a")).click();
    	
    }
    
    
@Test(priority =3,dataProvider = "carddata")
public void thankYouCrads(String edcon_card_number,String mobile_number)
{
	
driver.findElement(By.linkText("LOG IN")).click();
driver.findElement(By.id("edcon_card_number")).sendKeys(edcon_card_number);
driver.findElement(By.id("mobile_number")).sendKeys(mobile_number);
driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div/form/div[2]/div[3]/button")).click();
driver.manage().timeouts().implicitlyWait(2,TimeUnit.MINUTES);
//Assert.assertEquals("http://qa.edgars.co.za/customer/account", driver.getCurrentUrl());
}

@DataProvider(name = "carddata")
public Object[][] readExcel1() throws BiffException, IOException {
	// TODO Auto-generated method stub
	File f = new File("C:/Users/InventUser/Desktop/datadriven3.xls");
	Workbook w = Workbook.getWorkbook(f);
	Sheet s = w.getSheet("ThankYouCard");
	int rows = s.getRows();
    int columns = s.getColumns();
    //System.out.println(rows);
    //System.out.println(columns);
    
    String inputData[][] = new String [rows][columns];
       for(int i=0; i<rows; i++) {
    	for(int j=0; j<columns; j++){
    		Cell c = s.getCell(j,i);
    		inputData[i][j] = c.getContents();
    		System.out.println(inputData[i][j]);
    		}
    	}
   return inputData; 
}

@Test(priority=4)
public static void checkoutThankyouCardLinks(){
	driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[2]/div/div/div/div[8]/div/div/div/a/span/span[2]")).click();
	driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[2]/div/div/div/div[8]/div/div/div/div/div/div[3]/button[2]")).click();
	 driver.findElement(By.id("p_method_postilion_accountcard")).click();
     driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
     String verifytext = driver.findElement(By.id("Use another Thank U card? ")).getText();
     System.out.println(verifytext);
     
}
@AfterTest
public void Logout() throws InterruptedException{
	//driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	driver.findElement(By.linkText("LOG OUT")).click();
	String expected = "Edgars | latest fashion for women, men�s and kids, beauty & homeware";
	String actual = driver.getTitle();
	Assert.assertEquals(actual, expected);
	System.out.println(actual);
	driver.quit();
	}
}




