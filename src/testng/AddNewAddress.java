package testng;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AddNewAddress {
	public static  WebDriver driver;
	public String expected = null;
	public String actual = null;
	
    public static String baseUrl = "http://qa.edgars.co.za";

    @BeforeTest
    public static void launchBrowser(){
	System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");
	driver = new ChromeDriver();
    	//driver = new FirefoxDriver();
	driver.get(baseUrl);
	/*String expected = "Edgars | latest fashion for women, men�s and kids, beauty & homeware";
	String actual = driver.getTitle();
	Assert.assertEquals(actual, expected);
	System.out.println(actual);*/
	driver.manage().window().maximize();
}
    @Test(priority = 0)
    public static void logIn() throws InterruptedException{
    driver.findElement(By.linkText("LOG IN")).click();
    String expected1 = "Customer Login";
    String actual1 = driver.getTitle();
    Thread.sleep(3000);
    Assert.assertEquals(actual1, expected1);
    System.out.println(actual1);
    }
    
    @Test(priority = 1)
    public static void captureDeatils() throws InterruptedException{
    driver.findElement(By.id("email")).sendKeys("qatesting.commerce@gmail.com");
    driver.findElement(By.name("login[password]")).sendKeys("edgars#");
    driver.findElement(By.id("send2")).click();
    driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[1]/div/div/div[1]/div/div/div[2]/div[2]/div/div/div/div/ul/li[1]/a")).click();
   /* String expected = "My Account";
    String actual = driver.getTitle();
    Thread.sleep(3000);
    Assert.assertEquals(actual, expected);
    System.out.println(actual);*/
    } 
    
    @Test(priority=2)
    public static void addressBookValidation() throws InterruptedException{
    	
    	/*driver.findElements(By.xpath(".//ul/li/a"));   //click on addressbook link under my account
*/
    	driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[2]/div[2]/div/div[3]/div/div[2]/ul/li[3]/a")).click();  //click on addressbook link under my account
  
    	driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS); 
    	String expected2 = "Address Book";
    	    String actual2 = driver.getTitle();
    	    Thread.sleep(3000);
    	    Assert.assertEquals(actual2, expected2);
    	    System.out.println(actual2);
    	    if(actual2.equals(expected2)){
    	    	driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div/div[1]/button")).click(); //Clicking on 'addnew address button under address book
    	    }
    	    else
    	    	System.out.println("Unable to click on the addbook link under my account");
    	}
    
    	   @Test(priority= 3)
    		public static void contactInformation(){
    	    	
    	    	driver.findElement(By.id("company")).sendKeys("XSQLI");
    	    	driver.findElement(By.id("telephone")).sendKeys("0738454870");
    	    	driver.findElement(By.id("fax")).sendKeys("001");
    	    	
    		}

    	    @Test(priority= 4)
    		public static void Address (){
    	    	driver.findElement(By.id("street_1")).sendKeys("32 google street");
    	    	driver.findElement(By.id("street_2")).sendKeys("Long street1");
    	    	driver.findElement(By.id("city")).sendKeys("Cape town");
    	    	driver.findElement(By.id("region")).sendKeys("westren cape"); 
    	    	driver.findElement(By.id("zip")).sendKeys("8001"); 
  
    	    	WebElement address = driver.findElement(By.id("country"));  //Selecting option from drop down list for country
    	    	  Select objSelect = new Select(driver.findElement(By.id("country"))); 
    	    	  objSelect.selectByValue("ZA");
    	    	  driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS); 
    	    	driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div/form/div[3]/button")).click();
    	    	
    	    
    	    
    	   }

}
    		
    	
    