package testng;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class AccountInformation {
	
  public static WebDriver driver;
  
  @Test(dataProvider = "testdata")
	public static void validLogins(String username,String password,String newpassword) throws Exception
	{
	System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");	
	driver = new ChromeDriver();
	driver.get("http://qa.edgars.co.za/");
	driver.manage().window().maximize();
	driver.findElement(By.linkText("LOG IN")).click();
	driver.findElement(By.name("login[username]")).sendKeys(username);
	driver.findElement(By.name("login[password]")).sendKeys(password);
	driver.findElement(By.id("send2")).click();
	driver.manage().timeouts().implicitlyWait(2,TimeUnit.MINUTES);
	AccountInformation.changePassword(username,password,newpassword);
	}
	
	@AfterMethod  
	public void closeBrowser(){
		driver.close();
	}

    @DataProvider(name = "testdata")
	public Object[][] readExcel() throws BiffException, IOException {
		// TODO Auto-generated method stub
		File f = new File("C:/Users/InventUser/Desktop/datadriven3.xls");
		Workbook w = Workbook.getWorkbook(f);
		Sheet s = w.getSheet("Logins");
		int rows = s.getRows();
	    int columns = s.getColumns();
	    //System.out.println(rows);
	    //System.out.println(columns);
	    
	    String inputData[][] = new String [rows][columns];
           for(int i=0; i<rows; i++) {
	    	for(int j=0; j<columns; j++){
	    		Cell c = s.getCell(j,i);
	    		inputData[i][j] = c.getContents();
	    		System.out.println(inputData[i][j]);
	    		}
	    	}
	   return inputData; 
	}
 public static void accountInfo(){
	 driver.findElement(By.xpath("//a[@href='http://qa.edgars.co.za/customer/account/edit/']")).click();  //click on the account information link 
	         String actual1= driver.getCurrentUrl();
	         String expected1 = "Account Information"; 
	         Assert.assertEquals(actual1, expected1);
	         if(actual1.equals(expected1)){
	        	 
	         }
	         else{
	        	 System.out.println("Failed to click on account information");
	         }
 }
  public static void changePassword(String username,String password,String newpassword) throws Exception{
	driver.findElement(By.xpath("//a[@href='http://qa.edgars.co.za/customer/account/']")).click(); //clicking on my account
	String expected1 = "My Account";
    String actual1= driver.getTitle();
    Assert.assertEquals(actual1, expected1);
    if(actual1.equals(expected1)){
    	System.out.println("User placed under myaccount section");
    }
    else{
   	 System.out.println("Failed to click on account information");
    }
   try{
    driver.findElement(By.xpath("//a[@href='http://qa.edgars.co.za/customer/account/edit/changepass/1/']")).click();
  
    	System.out.println("user palced under change password section");
    }
   
   catch(Exception e){
	   System.out.println(e.getMessage());
   }
	   /*String expected = "My Details";
	   String actual=driver.getTitle();
			 Assert.assertEquals(actual, expected);
			 System.out.println(actual);*/
			 driver.findElement(By.id("current_password")).sendKeys(password);
			 driver.findElement(By.id("password")).sendKeys(newpassword);
			 driver.findElement(By.id("confirmation")).sendKeys(newpassword);
			 driver.findElement(By.xpath("//button[@title='Save']")).click();
			 try{
				 WebElement ele =driver.findElement(By.xpath("//span[text()='The account information has been saved.']"));
				 if(ele.isDisplayed()){
					 driver.findElement(By.xpath("//a[@title='Log Out']")).click();
					 AccountInformation.reLogin(username, newpassword,password);
				 }
			 }
			 catch(Exception e1){
				 System.out.println("Program not executed completely cause of" + e1.getMessage());
			
			}
			}
            public static void reLogin(String username,String password,String newpassword) throws Exception{
			driver.findElement(By.name("login[username]")).sendKeys(username);
			driver.findElement(By.name("login[password]")).sendKeys(newpassword);
			driver.findElement(By.id("send2")).click();
            driver.manage().timeouts().implicitlyWait(2,TimeUnit.MINUTES);
             AccountInformation.rechangePassword(username,password,newpassword);
            
            }
        
            
         public static void rechangePassword(String username,String password,String newpassword) throws Exception{
           	driver.findElement(By.xpath("//a[@href='http://qa.edgars.co.za/customer/account/']")).click(); //clicking on my account
           	String expected1 = "My Account";
               String actual1= driver.getTitle();
               Assert.assertEquals(actual1, expected1);
               if(actual1.equals(expected1)){
               	System.out.println("User placed under myaccount section");
               }
               else{
              	 System.out.println("Failed to click on account information");
               }
              try{
               driver.findElement(By.xpath("//a[@href='http://qa.edgars.co.za/customer/account/edit/changepass/1/']")).click();
             
               	System.out.println("user palced under change password section");
               }
              
              catch(Exception e){
           	   System.out.println(e.getMessage());
              }
           	   /*String expected = "My Details";
           	   String actual=driver.getTitle();
           			 Assert.assertEquals(actual, expected);
           			 System.out.println(actual);*/
           			 driver.findElement(By.id("password")).sendKeys(password);
           			 driver.findElement(By.id("confirmation")).sendKeys(password);
           			 driver.findElement(By.xpath("//button[@title='Save']")).click();
           			 try{
           				 WebElement ele =driver.findElement(By.xpath("//span[text()='The account information has been saved.']"));
           				 if(ele.isDisplayed()){
           					 driver.findElement(By.xpath("//a[@title='Log Out']")).click();
           					 AccountInformation.reLogin(username,newpassword,password);
           				 }
           			 }
           			 catch(Exception e1){
           				 System.out.println("Program not executed completely cause of" + e1.getMessage());
           			
           			}
           			}
				
			}
   
   
	   
   
   
    


 
