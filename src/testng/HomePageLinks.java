package testng;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class HomePageLinks {
  
	
	public static  WebDriver driver;
	public String expected = null;
	public String actual = null;
	public static String baseUrl = "http://qa.edgars.co.za";
	
	
	 @BeforeTest
   public static void launchBrowser(){
		System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");
		driver = new ChromeDriver();
		 //driver = new FirefoxDriver();
		driver.get(baseUrl);
		String expected = "Edgars | latest fashion for women, men�s and kids, beauty & homeware";
		String actual = driver.getTitle();
		Assert.assertEquals(actual, expected);
		System.out.println(actual);
		driver.manage().window().maximize();
		
		
	 }
	 @Test(priority=0)
  public static void headerLinks() {
		 
	  driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[1]/div/div/div[2]/div[2]/div/div/div/div/ul/li[1]/a")).click();  //store locator
	  driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	  driver.findElement(By.xpath(".//html/body/div[3]/div[1]/span")).click();  //close button on store locator
	  driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	  driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[1]/div/div/div[2]/div[2]/div/div/div/div/ul/li[2]")).click();  //clicking on wish list
	  driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
      driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[1]/div/div/div[2]/div[2]/div/div/div/div/ul/li[3]/a")).click();   //clicking on login   
      driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
      driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[1]/div/div/div[2]/div[2]/div/div/div/div/ul/li[4]/a")).click();  //clicking on register button
      driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
      driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[2]/div/div/div/div[6]/div/div/a/img")).click();  //edgars logo
 

	 }
		
	
   @Test(priority=1)
 public static void middleLinks(){
		    driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[3]/div/div[2]/a/img")).click();  //women static block
		    boolean refresh = utility.ReUsable.refreshPage("Women", driver, 15);
		    driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[2]/div/div/div/div[6]/div/div/a/img")).click();  //edgars logo
		    driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[3]/div/div[3]/a/img")).click();  //men static block
		    boolean refresh1 = utility.ReUsable.refreshPage("Men", driver, 15);
		    driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[2]/div/div/div/div[6]/div/div/a/img")).click();  //edgars logo
		    driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[3]/div/div[4]/a/img")).click(); //kids static block
		    boolean refresh2 = utility.ReUsable.refreshPage("Kids", driver, 15);
		    driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[2]/div/div/div/div[6]/div/div/a/img")).click();  //edgars logo
		    driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
   }
		
 
  @Test(priority = 2)
  public static void footerIcons(){
			driver.findElement(By.xpath(".//html/body/div/div/div/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/div[1]/a")).click();    //Facebook icon
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.findElement(By.xpath(".//html/body/div/div/div/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/div[2]/a")).click();    //Twitter icon"
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		    driver.findElement(By.xpath(".//html/body/div/div/div/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/div[3]/a")).click();		//Instagram
		    driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		    driver.findElement(By.xpath(".//html/body/div/div/div/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/div[4]/a")).click();    //Youtube
		    driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }
    
@Test(priority =3)
public static void moreFromUs() {
				driver.findElement(By.xpath(".//html/body/div/div/div/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div/div/div/ul/li[1]/a")).click(); //join edgars club link
				//boolean refresh4 = utility.ReUsable.refreshPage("One Step Checkout", driver, 15);
				driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				driver.findElement(By.xpath(".//html/body/div/div/div/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div/div/div/ul/li[2]/a")).click(); // insurance link
				// boolean refresh5 = utility.ReUsable.refreshPage("One Step Checkout", driver, 15);
				driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				driver.findElement(By.xpath(".//html/body/div/div/div/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div/div/div/ul/li[3]/a")).click(); //cellular		
				//boolean refresh6 = utility.ReUsable.refreshPage("One Step Checkout", driver, 15);
				driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			}
		
 @Test(priority =4)
 public static void needHelp(){
			driver.findElement(By.xpath(".//html/body/div/div/div/div[3]/div/div/div[2]/div/div/div[1]/div[3]/div/div/div/ul/li[1]/a")).click(); //Deliver information
			 boolean refresh7 = utility.ReUsable.refreshPage("Delivery Information", driver, 15);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.findElement(By.xpath(".//html/body/div/div/div/div[3]/div/div/div[2]/div/div/div[1]/div[3]/div/div/div/ul/li[2]/a")).click();  //Faqs
			 boolean refresh8 = utility.ReUsable.refreshPage("FAQS", driver, 15);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.findElement(By.xpath(".//html/body/div/div/div/div[3]/div/div/div[2]/div/div/div[1]/div[3]/div/div/div/ul/li[3]/a")).click(); //Find a store"

			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.findElement(By.xpath(".//html/body/div[3]/div[1]/span")).click();  //close button on store locator
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.findElement(By.xpath(".//html/body/div/div/div/div[3]/div/div/div[2]/div/div/div[1]/div[3]/div/div/div/ul/li[4]/a")).click(); //Talk to us	
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		}
		
  @Test(priority =5)
 public static void theSmallPrint(){
			driver.findElement(By.xpath(".//html/body/div/div/div/div[3]/div/div/div[2]/div/div/div[1]/div[4]/div/div/div/ul/li[1]/a")).click();  //terms&conditions
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.findElement(By.xpath(".//html/body/div/div/div/div[3]/div/div/div[2]/div/div/div[1]/div[4]/div/div/div/ul/li[2]/a")).click(); //policies
			 boolean refresh9 = utility.ReUsable.refreshPage("Privacy Policy", driver, 15);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[2]/div/div/div/div[6]/div/div/a/img")).click(); //edgars logo
			 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//driver.findElement(By.xpath(".//html/body/div/div/div/div[3]/div/div/a")).click(); //top navigation
			 //driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
					
		}

  @AfterTest
  public void Logout() throws InterruptedException{
  	driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
  	driver.quit();
  	}
  }



  