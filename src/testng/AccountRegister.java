package testng;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class AccountRegister {
	
	public static  WebDriver driver;
	public String expected = null;
	public String actual = null;
	public static String baseUrl = "http://qa.edgars.co.za";
	
	 @BeforeTest
	    public static void launchBrowser(){
		System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");
		driver = new ChromeDriver();
		 //driver = new FirefoxDriver();
		driver.get(baseUrl);
		String expected = "Edgars | latest fashion for women, men�s and kids, beauty & homeware";
		String actual = driver.getTitle();
		Assert.assertEquals(actual, expected);
		System.out.println(actual);
		driver.manage().window().maximize();
	}
	
	@Test(priority = 0)
     
	 public static void registerBtn(){
	 driver.findElement(By.linkText("REGISTER")).click();
	 String actual_title = driver.getTitle();   // verifying page title
	 System.out.println("page title:" +actual_title);
	 Assert.assertTrue(actual_title.contains("Create New Customer Account"));
	 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 driver.findElement(By.linkText("LOG IN")).click(); //login link on register page
	 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 driver.findElement(By.linkText("REGISTER")).click();  //Clicking on home page link
	
	}
	
	@Test(priority= 1)
		public static void regeisterRequired(){
			
		String actual_title1 = driver.getTitle();   // verifying page title
		 System.out.println("page title:" +actual_title1);
		 Assert.assertTrue(actual_title1.contains("Create New Customer Account"));
	     driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	     driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[2]/div[2]/div/div[3]/div/form/div[3]/button")).click();
	     String errorm1 = driver.findElement(By.id("advice-required-entry-firstname")).getText(); 
	     String errorm2 = driver.findElement(By.id("advice-required-entry-lastname")).getText();
	     String errorm3 = driver.findElement(By.id("advice-required-entry-email_address")).getText();
	     String errorm4 = driver.findElement(By.id("advice-required-entry-password")).getText();
	     String errorm5 = driver.findElement(By.id("advice-required-entry-confirmation")).getText();
	     
	     System.out.println(errorm1);
	     System.out.println(errorm2);
	     System.out.println(errorm3);
	     System.out.println(errorm4);
	     System.out.println(errorm5);
	    
		}
	
	@Test(priority = 2,dataProvider = "testdata")
	public static void regiester(String firstname,String lastname, String emailaddress,String password) throws IOException
	{
	driver.findElement(By.linkText("REGISTER")).click();
	driver.findElement(By.name("firstname")).sendKeys(firstname);
	driver.findElement(By.name("lastname")).sendKeys(lastname);
	driver.findElement(By.name("email")).sendKeys(emailaddress);
	driver.findElement(By.name("password")).sendKeys(password);
	driver.findElement(By.name("confirmation")).sendKeys(password);
	driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[2]/div[2]/div/div[3]/div/form/div[3]/button")).click();
	driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	TakesScreenshot ts1 =(TakesScreenshot)driver;   	//screen shot capturing
	File source1 = ts1.getScreenshotAs(OutputType.FILE);
	String screenShotFName = "./AccountRegister/" + emailaddress +"AccountRegister.png";
	FileUtils.copyFile(source1, new File(screenShotFName));
	System.out.println("screen shot taken");   //screen shot capturing end
	driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	driver.findElement(By.linkText("LOG OUT")).click();
	//driver.findElement(By.linkText("REGISTER")).click();
		
	}
	
	@DataProvider(name = "testdata")
	public Object[][] readExcel() throws BiffException, IOException {
		// TODO Auto-generated method stub
	File f = new File("C:/Users/InventUser/Desktop/datadriven3.xls");
	Workbook w = Workbook.getWorkbook(f);
	Sheet s = w.getSheet("AccountRegister");
	int rows = s.getRows();
	int columns = s.getColumns();
	   	    
	    String inputData[][] = new String [rows][columns];
           for(int i=0; i<rows; i++) {
	    	for(int j=0; j<columns; j++){
	    		Cell c = s.getCell(j,i);
	    		inputData[i][j] = c.getContents();
	    		System.out.println(inputData[i][j]);
	    		}
	    	}
	   return inputData; 
	}
	
	@AfterTest
		   public static void Logout() throws InterruptedException{
			/*driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			String expected = "Edgars | latest fashion for women, men�s and kids, beauty & homeware";
			String actual = driver.getTitle();
			Assert.assertEquals(actual, expected);
			System.out.println(actual);*/
			driver.quit();
			}
}
