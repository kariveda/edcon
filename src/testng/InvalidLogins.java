package testng;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class InvalidLogins {
	
public WebDriver driver;
	
@Test(priority = 1)
public void openBrowser(){
	System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");	
	driver = new ChromeDriver();
	driver.get("http://qa.edgars.co.za");
}

@Test(priority=2, dataProvider = "testdata")
public void invalidLogin(String username,String password) throws IOException, InterruptedException
{
driver.manage().window().maximize();
driver.findElement(By.linkText("LOG IN")).click();
driver.findElement(By.name("login[username]")).sendKeys(username);
driver.findElement(By.name("login[password]")).sendKeys(password);
driver.findElement(By.id("send2")).click();

Thread.sleep(2000);

//screen shot capturing
	 TakesScreenshot ts1 =(TakesScreenshot)driver;
	 File source = ts1.getScreenshotAs(OutputType.FILE);
	 driver.findElement(By.name("login[username]"));
	 String screenShotFName = "./Invalidlogins/" + username + "/valid.png";
	 FileUtils.copyFile(source, new File(screenShotFName));
	 System.out.println("screen shot taken");
	//screen shot capturing end
}	 
@DataProvider(name = "testdata")
public Object[][] readExcel() throws BiffException, IOException {
	// TODO Auto-generated method stub
	File f = new File("C:/Users/InventUser/Desktop/datadriven3.xls");
	Workbook w = Workbook.getWorkbook(f);
	Sheet s = w.getSheet("InvalidLogins");
	int rows = s.getRows();
    int columns = s.getColumns();
    //System.out.println(rows);
    //System.out.println(columns);
    String inputData[][] = new String [rows][columns];
       for(int i=0; i<rows; i++){
    	for(int j=0; j<columns; j++){
    		Cell c = s.getCell(j,i);
    		inputData[i][j] = c.getContents();
    		System.out.println(inputData[i][j]);
    		}
          }
    	   return inputData; 
}
@Test(priority = 3)
public void Logout() throws InterruptedException{
	driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	/*String expected = "Edgars | latest fashion for women, men�s and kids, beauty & homeware";
	String actual = driver.getTitle();
	Assert.assertEquals(actual, expected);
	System.out.println(actual);*/
	driver.quit();
	}
}
