package testng;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class MyAccountLinks {
  

    
	public static  WebDriver driver;
	public String expected = null;
	public String actual = null;
	
    public static String baseUrl = "http://qa.edgars.co.za";


    @BeforeTest
    public static void launchBrowser(){
	System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");
	driver = new ChromeDriver();
	driver.get(baseUrl);
	String expected = "Edgars | latest fashion for women, men�s and kids, beauty & homeware";
	String actual = driver.getTitle();
	Assert.assertEquals(actual, expected);
	System.out.println(actual);
	driver.manage().window().maximize();
}

@Test(priority = 0)
public static void logIn() throws InterruptedException{
driver.findElement(By.linkText("LOG IN")).click();
String expected = "Customer Login";
String actual = driver.getTitle();
Thread.sleep(3000);
Assert.assertEquals(actual, expected);
System.out.println(actual);
}

@Test(priority = 1)
public static void captureDeatils() throws InterruptedException{
driver.findElement(By.id("email")).sendKeys("qatesting.commerce@gmail.com");
driver.findElement(By.name("login[password]")).sendKeys("edgars#");
driver.findElement(By.id("send2")).click();
String expected = "My Account";
String actual = driver.getTitle();
Thread.sleep(3000);
Assert.assertEquals(actual, expected);
System.out.println(actual);
} 

@Test(priority = 2)
public static void accountInformation(){
driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[2]/div[2]/div/div[3]/div/div[2]/ul/li[2]/a")).click(); //Click on account information
Assert.assertEquals("http://qa.edgars.co.za/customer/account/edit/", driver.getCurrentUrl());
System.out.println(driver.getCurrentUrl());
}

@Test(priority =3)
public static void addressBook(){
driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[3]/div/div[2]/ul/li[3]/a")).click(); //Click on account information
Assert.assertEquals("http://qa.edgars.co.za/customer/address/", driver.getCurrentUrl());
System.out.println(driver.getCurrentUrl());
}

@Test(priority =4)
public static void MyOrders(){
driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[3]/div/div[2]/ul/li[4]/a")).click(); //Click on account information
Assert.assertEquals("http://qa.edgars.co.za/sales/order/history/", driver.getCurrentUrl());
System.out.println(driver.getCurrentUrl());
}

@Test(priority =5)
public static void MyWishlist(){
driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[3]/div/div[2]/ul/li[5]/a")).click(); //Click on account information
Assert.assertEquals("http://qa.edgars.co.za/wishlist/", driver.getCurrentUrl());
System.out.println(driver.getCurrentUrl());
}

@Test(priority =6)
public static void NewsletterSubscription(){
driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[3]/div/div[2]/ul/li[6]/a")).click(); //Click on account information
Assert.assertEquals("http://qa.edgars.co.za/newsletter/manage/", driver.getCurrentUrl());
System.out.println(driver.getCurrentUrl());
}

@Test(priority =7)
public static void thankUCard(){
driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[3]/div/div[2]/ul/li[7]/a")).click(); //Click on account information
Assert.assertEquals("http://qa.edgars.co.za/accountcard/customer_account/linkacc/", driver.getCurrentUrl());
System.out.println(driver.getCurrentUrl());
}

@Test(priority =8)
public static void mySummary(){
driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[3]/div/div[2]/ul/li[8]/a")).click(); //Click on account information
Assert.assertEquals("http://qa.edgars.co.za/customer/accountpage/", driver.getCurrentUrl());
System.out.println(driver.getCurrentUrl());
}

@Test(priority =9)
public static void myStatement(){
driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[3]/div/div[2]/ul/li[9]/a")).click(); //Click on account information
Assert.assertEquals("http://qa.edgars.co.za/customer/accountpage/statement/", driver.getCurrentUrl());
System.out.println(driver.getCurrentUrl());
}

@AfterMethod
public void screenShotCapture() throws IOException{
	String name = driver.getTitle();
	TakesScreenshot ts1 =(TakesScreenshot)driver;
	 File source = ts1.getScreenshotAs(OutputType.FILE);
	 String screenShotFName = "./MyAccount/" + name + "/link.png";
	 FileUtils.copyFile(source, new File(screenShotFName));
	 System.out.println("screen shot taken");
	
}

@AfterTest
public void Logout() throws InterruptedException{
	driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	driver.findElement(By.linkText("LOG OUT")).click();
	String expected = "Edgars | latest fashion for women, men�s and kids, beauty & homeware";
	String actual = driver.getTitle();
	Assert.assertEquals(actual, expected);
	System.out.println(actual);
	driver.quit();
	}
}




