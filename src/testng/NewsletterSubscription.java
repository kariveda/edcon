package testng;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class NewsletterSubscription {
	public static  WebDriver driver;
	public String expected = null;
	public String actual = null;
	
    public static String baseUrl = "http://qa.edgars.co.za";


    @BeforeTest
    public static void launchBrowser(){
	System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");
	driver = new ChromeDriver();
	//driver = new FirefoxDriver();
    driver.get(baseUrl);
	String expected = "Edgars | latest fashion for women, men�s and kids, beauty & homeware";
	String actual = driver.getTitle();
	Assert.assertEquals(actual, expected);
	System.out.println(actual);
	driver.manage().window().maximize();
}
	@Test(priority = 0)
	public static void logIn() throws InterruptedException{
	driver.findElement(By.linkText("LOG IN")).click();
	String expected = "Customer Login";
	String actual = driver.getTitle();
	Thread.sleep(3000);
	Assert.assertEquals(actual, expected);
	System.out.println(actual);
	}

	@Test(priority = 1)
	public static void captureDeatils() throws InterruptedException{
	driver.findElement(By.id("email")).sendKeys("qatesting.commerce@gmail.com");
	driver.findElement(By.name("login[password]")).sendKeys("edgars#");
	driver.findElement(By.id("send2")).click();
	String expected = "My Account";
	String actual = driver.getTitle();
	Thread.sleep(3000);
	Assert.assertEquals(actual, expected);
	System.out.println(actual);
	}
	


@Test(priority=2)
public static void subscriptionStatus() throws IOException{
	driver.findElement(By.xpath("//a[@href='http://qa.edgars.co.za/newsletter/manage/']")).click();
    String expected = "Newsletter Subscription";
	String actual = driver.getTitle();
	Assert.assertEquals(actual,expected);
	WebElement checkbox = driver.findElement(By.xpath("//input[@type='checkbox' and @title = 'General Subscription']"));
	//if(actual.equals(expected)){
	if(!checkbox.isSelected()){
		driver.findElement(By.xpath("//input[@type='checkbox' and @title='General Subscription']")).click();
		driver.findElement(By.xpath("//button[@type='submit' and @title='Save']")).click();
		
	}else{
			System.out.println("User already subscribed news letters");
		}
		 //screen shot capturing start
		 TakesScreenshot ts =(TakesScreenshot)driver;
		 File source= ts.getScreenshotAs(OutputType.FILE);
		 FileUtils.copyFile(source, new File("./NewsLetters/Status.png"));
		 System.out.println("screen shot taken");
		 //screen shot capturing end
		 String expected1 = "My Account";
		 String actual1 = driver.getTitle();
		 Assert.assertEquals(actual, expected);
        System.out.println("User subscribed news letters and placed under" + actual1);
 
	}
	
@AfterTest
public static void Logout() throws InterruptedException{
	driver.findElement(By.linkText("LOG OUT")).click();
	driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	String expected = "Edgars | latest fashion for women, men�s and kids, beauty & homeware";
	String actual = driver.getTitle();
	Assert.assertEquals(actual, expected);
	System.out.println(actual);
	driver.quit();
	}
}
		
	
