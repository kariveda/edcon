package testng;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class Tabs {
	
	public static WebDriver driver;
	 
	@BeforeSuite
	 public void openBrowser(){
		System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");	
		driver = new ChromeDriver();
		//driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://qa.edgars.co.za");
		}
	
	@AfterSuite
	
    public static void closeBrowser(){
					driver.close();
	}
   @Test(priority=1)
    public void womenTabClick() throws IOException{
	driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[3]/div/div/ul/li[4]/a/span[1]")).click();  //clicking on womens tab
	//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 boolean refresh = utility.ReUsable.refreshPage("Women", driver, 15);
     String actual_title = driver.getTitle();   // verifying page title
	 System.out.println("page title:" +actual_title);
	 Assert.assertTrue(actual_title.contains("Women")); 
	 
	 TakesScreenshot ts =(TakesScreenshot)driver;   //screen shot
	 File source= ts.getScreenshotAs(OutputType.FILE);
	 FileUtils.copyFile(source, new File("./Tabs/Women.png"));
	 System.out.println("screen shot taken");    //screen shot end

   }
   @Test(priority=2)
   public void menTabClick() throws IOException{
	driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[3]/div/div/ul/li[5]/a/span[1]")).click();  //clicking on womens tab
	//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	boolean refresh1 = utility.ReUsable.refreshPage("Men", driver, 15);
    String actual_title1 = driver.getTitle();   // verifying page title
	 System.out.println("page title:" +actual_title1);
	 Assert.assertTrue(actual_title1.contains("Men")); 
	 
	 TakesScreenshot ts1 =(TakesScreenshot)driver;   //screen shot
	 File source1= ts1.getScreenshotAs(OutputType.FILE);
	 FileUtils.copyFile(source1, new File("./Tabs/Men.png"));
	 System.out.println("screen shot taken");    //screen shot end
	
   }
   @Test(priority=3)
   public void kidsTabClick() throws IOException{
	driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[3]/div/div/ul/li[6]/a/span[1]")).click();  //clicking on womens tab
	//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	boolean refresh2 = utility.ReUsable.refreshPage("Kids", driver, 15);
    String actual_title2 = driver.getTitle();   // verifying page title
	 System.out.println("page title:" +actual_title2);
	 Assert.assertTrue(actual_title2.contains("Kids")); 
	 
	 TakesScreenshot ts2 =(TakesScreenshot)driver;   //screen shot
	 File source2= ts2.getScreenshotAs(OutputType.FILE);
	 FileUtils.copyFile(source2, new File("./Tabs/Kids.png"));
	 System.out.println("screen shot taken");    //screen shot end
	
   }
   
   @Test(priority=4)
   public void shoesTabClick() throws IOException{
	driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[3]/div/div/ul/li[7]/a/span[1]")).click();  //clicking on womens tab
	//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	boolean refresh3 = utility.ReUsable.refreshPage("Shoes", driver, 15);
    String actual_title3 = driver.getTitle();   // verifying page title
	 System.out.println("page title:" +actual_title3);
	 Assert.assertTrue(actual_title3.contains("Shoes")); 
	 
	 TakesScreenshot ts3 =(TakesScreenshot)driver;   //screen shot
	 File source3= ts3.getScreenshotAs(OutputType.FILE);
	 FileUtils.copyFile(source3, new File("./Tabs/Shoes.png"));
	 System.out.println("screen shot taken");    //screen shot end
	
   }
   @Test(priority=5)
   public void beautyTabClick() throws IOException{
	driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[3]/div/div/ul/li[8]/a/span[1]")).click();  //clicking on womens tab
	//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	boolean refresh4 = utility.ReUsable.refreshPage("Beauty", driver, 15);
    String actual_title4 = driver.getTitle();   // verifying page title
	 System.out.println("page title:" +actual_title4);
	 Assert.assertTrue(actual_title4.contains("Beauty")); 
	 
	 TakesScreenshot ts4 =(TakesScreenshot)driver;   //screen shot
	 File source4= ts4.getScreenshotAs(OutputType.FILE);
	 FileUtils.copyFile(source4, new File("./Tabs/Beauty.png"));
	 System.out.println("screen shot taken");    //screen shot end
	
   }
   
   @Test(priority=6)
   public  void homewareTabClick() throws IOException{
	driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[3]/div/div/ul/li[9]/a/span[1]")).click();  //clicking on womens tab
	//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	boolean refresh5 = utility.ReUsable.refreshPage("Homeware", driver, 15);
    String actual_title5 = driver.getTitle();   // verifying page title
	 System.out.println("page title:" +actual_title5);
	 Assert.assertTrue(actual_title5.contains("Homeware")); 
	 
	 TakesScreenshot ts5 =(TakesScreenshot)driver;   //screen shot
	 File source5= ts5.getScreenshotAs(OutputType.FILE);
	 FileUtils.copyFile(source5, new File("./Tabs/Homeware.png"));
	 System.out.println("screen shot taken");    //screen shot end
	
   }

   @Test(priority=7)
   public void cellularTabClick() throws IOException{
	driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[3]/div/div/ul/li[10]/a/span")).click();  //clicking on womens tab
	//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	boolean refresh6 = utility.ReUsable.refreshPage("Cellular", driver, 15);
    String actual_title6 = driver.getTitle();   // verifying page title
	 System.out.println("page title:" +actual_title6);
	 Assert.assertTrue(actual_title6.contains("Cellular")); 
	 
	 TakesScreenshot ts6 =(TakesScreenshot)driver;   //screen shot
	 File source6= ts6.getScreenshotAs(OutputType.FILE);
	 FileUtils.copyFile(source6, new File("./Tabs/Cellular.png"));
	 System.out.println("screen shot taken");    //screen shot end
	
   }
   @Test(priority=8)
   public void brandsTabClick() throws IOException{
	driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[3]/div/div/ul/li[11]/a/span[1]")).click();  //clicking on womens tab
	//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	boolean refresh7 = utility.ReUsable.refreshPage("Brands", driver, 15);
    String actual_title7 = driver.getTitle();   // verifying page title
	 System.out.println("page title:" +actual_title7);
	 Assert.assertTrue(actual_title7.contains("Brands")); 
	 
	 TakesScreenshot ts7 =(TakesScreenshot)driver;   //screen shot
	 File source7= ts7.getScreenshotAs(OutputType.FILE);
	 FileUtils.copyFile(source7, new File("./Tabs/Brands.png"));
	 System.out.println("screen shot taken");    //screen shot end
   
   }
   
   
}
	
	
	
	
	
	
	

		
		
		
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*if(condition true){
	verify page title
	take screen shot
	
	}
	else{
		jump to another satement and fail this one
	}
	driver.findElement(By.xpath("men")).click();
	if(condition true){
	verify page title
	take screen shot
	
	}
	else{
		jump to another statement and fail this one
	}
	
	
	driver.findElement(By.xpath("kids")).click();
	if(condition true){
	verify page title
	take screen shot
	
	}
	else{
		jump to another stateement and fail this one
	}
	driver.findElement(By.xpath("kids")).click();
	if(condition true){
	verify page title
	take screen shot
	
	}
	else{
		jump to another statement and fail this one
	}*/

		
		  
		  
		  
		  
	  
  

