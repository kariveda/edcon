package testng;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CreditCard 
{
//	@Test(priority = 0)
//	public void login() throws InterruptedException, IOException {
//	System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");
//	WebDriver driver = new ChromeDriver();
//	//WebDriver driver = new FirefoxDriver();
//	  driver.get("http://qa.edgars.co.za/");
//	  driver.manage().window().maximize();
//	  driver.findElement(By.linkText("LOG IN")).click(); 
//	  
//     driver.findElement(By.name("login[username]")).sendKeys("qatesting.commerce@gmail.com");
//	  driver.findElement(By.name("login[password]")).sendKeys("edgars#");
//	  driver.findElement(By.id("send2")).click();
//	  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//	 String actual_title = driver.getTitle();   // verifying page title
//	  System.out.println("page title:" +actual_title);
//	  Assert.assertTrue(actual_title.contains("My Account"));
//	  driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[3]/div/div/ul/li[4]/a/span[1]")).click();                    //Clicking on womens tab
//	  driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div[1]/div[2]/ul/li[1]/ul/li[1]/a")).click();          //Clicking on the new arrivals sub tab
//	  driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[5]/div[2]/ul/li/h2/a")).click();                         //Clicking on the product "Format shirt SKU: 20077238" product
//	  driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
//	  driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[2]/div[2]/div/div[4]/div[2]/form/div[4]/div[4]/div/button")).click();  //Adding product to the cart
//	  driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
//	  driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[1]/div/div/div[2]/div/div/div/div[8]/div/div/div/a/span/span[2]")).click();  //Click on the cart symbol
//	  driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
//	  driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[1]/div/div/div[2]/div/div/div/div[8]/div/div/div/a/span/span[2]")).click();  //Click on the cart symbol
//	  driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[1]/div/div/div[2]/div/div/div/div[8]/div/div/div/div/div[2]/button[2]")).click();      //click on the check out button under cart
//	  Thread.sleep(2000);   
//	  String actual_title1 = driver.getTitle();   // verifying page title
//	  System.out.println("page title:" +actual_title1);
//	  Assert.assertTrue(actual_title1.contains("One Step Checkout"));         //user placed in checkout section
//	//WebElement address = driver.findElement(By.id("shipping-address-select"));  //Selecting second option from drop down list under 'deliver to' section
//		  Select objSelect = new Select(driver.findElement(By.id("shipping-address-select")));
//	      objSelect.selectByIndex(1);
//	       
//	      List<WebElement> radio=driver.findElements(By.xpath("//html/body/div[1]/div/div/div[2]/div[2]/div/div[3]/form/fieldset/div/div[2]/div[1]/div/dl/dt[1]/input"));  //For selecting shipping method WOFS/Click and collect
//	      for(int i=0;i<radio.size();i++)
//	      {
//	    	  WebElement local_radio=radio.get(i);
//	    	  String value=local_radio.getAttribute("value");
//	    	  System.out.println("Shipping method is" +value);
//	    	  
//	    	  if(value.equalsIgnoreCase("wofs_wofs"))
//	    	  {
//	    		  local_radio.click();
//	    	  }
//	      
//	    	 
//	   driver.findElement(By.id("p_method_postilion_creditcard")).click();   // selecting credit card radio button under payment method
//	   Select objSelect1 = new Select(driver.findElement(By.id("postilion_creditcard_cc_type")));
//	   objSelect1.selectByValue("MC");
//	   driver.findElement( By.id("postilion_creditcard_cc_number")).sendKeys("5413330089010475");
//	   driver.findElement(By.id("postilion_creditcard_cc_owner")).sendKeys("anu");
//	   Select objSelect2 = new Select(driver.findElement(By.id("postilion_creditcard_expiration")));
//	   objSelect2.selectByValue("12");
//	   Select objSelect3 = new Select(driver.findElement(By.id("postilion_creditcard_expiration_yr")));
//	   objSelect3.selectByValue("2025");	  
//	   driver.findElement(By.id("postilion_creditcard_cc_cid")).sendKeys("160");
//	   driver.findElement(By.id("onestepcheckout-place-order")).click();
//	   driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//	   driver.switchTo().frame("centinel_authenticate_iframe");
//	   driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//	   driver.findElement(By.name("external.field.password")).sendKeys("test123");
//	   driver.findElement(By.name("UsernamePasswordEntry")).click();   
//	   //screen shot capturing start
//		 TakesScreenshot ts =(TakesScreenshot)driver;
//		 File source= ts.getScreenshotAs(OutputType.FILE);
//		 FileUtils.copyFile(source, new File("./Loginrequired/Requiredfeilds.png"));
//		 System.out.println("screen shot taken");
//		 //screen shot capturing end
//	      
//	      }
//	      }
//	}
      
	public static  WebDriver driver;
	public String expected = null;
	public String actual = null;
	
    public static String baseUrl = "http://qa.edgars.co.za";


    @BeforeTest
    public static void launchBrowser(){
	System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");
	driver = new ChromeDriver();
	driver.get(baseUrl);
	String expected = "Edgars | latest fashion for women, men�s and kids, beauty & homeware";
	String actual = driver.getTitle();
	Assert.assertEquals(actual, expected);
	System.out.println(actual);
	driver.manage().window().maximize();
}

@Test(priority = 0)
public static void logIn() throws InterruptedException{
driver.findElement(By.linkText("LOG IN")).click();
String expected = "Customer Login";
String actual = driver.getTitle();
Thread.sleep(3000);
Assert.assertEquals(actual, expected);
System.out.println(actual);
}

@Test(priority = 1)
public static void captureDeatils() throws InterruptedException{
driver.findElement(By.id("email")).sendKeys("qatesting.commerce@gmail.com");
driver.findElement(By.name("login[password]")).sendKeys("edgars#");
driver.findElement(By.id("send2")).click();
String expected = "My Account";
String actual = driver.getTitle();
Thread.sleep(3000);
Assert.assertEquals(actual, expected);
System.out.println(actual);
} 

/**
 * @throws InterruptedException
 * @throws IOException 
 */
@Test(priority = 2)
public static void addToCart() throws InterruptedException, IOException{
	
driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[3]/div/div/ul/li[4]/a/span[1]")).click();                    //Clicking on womens tab
driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div[1]/div[2]/ul/li[1]/ul/li[1]/a")).click();          //Clicking on the new arrivals sub tab
driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[5]/div[2]/ul/li/h2/a")).click();                         //Clicking on the product "Format shirt SKU: 20077238" product
driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[2]/div[2]/div/div[4]/div[2]/form/div[4]/div[4]/div/button")).click();  //Adding product to the cart
Thread.sleep(3000);
driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[1]/div/div/div[2]/div/div/div/div[8]/div/div/div/a/span/span[2]")).click();  //Click on the cart symbol
driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[1]/div/div/div[2]/div/div/div/div[8]/div/div/div/a/span/span[2]")).click();  //Click on the cart symbol
driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[1]/div/div/div[2]/div/div/div/div[8]/div/div/div/div/div[2]/button[2]")).click();      //click on the check out button under cart
Thread.sleep(2000);   
String actual_title1 = driver.getTitle();   // verifying page title
System.out.println("page title:" +actual_title1);
Assert.assertTrue(actual_title1.contains("One Step Checkout"));         //user placed in checkout section
WebElement address = driver.findElement(By.id("shipping-address-select"));  //Selecting second option from drop down list under 'deliver to' section
  Select objSelect = new Select(driver.findElement(By.id("shipping-address-select")));
  objSelect.selectByIndex(1);
       List<WebElement> radio=driver.findElements(By.xpath("//html/body/div[1]/div/div/div[2]/div[2]/div/div[3]/form/fieldset/div/div[2]/div[1]/div/dl/dt[1]/input"));  //For selecting shipping method WOFS/Click and collect
  for(int i=0;i<radio.size();i++)
   {
 	  WebElement local_radio=radio.get(i);
  	  String value=local_radio.getAttribute("value");
  	  System.out.println("Shipping method is" +value);
 	  
 	  if(value.equalsIgnoreCase("wofs_wofs"))  	  {
 		  local_radio.click();
 	  }
   }
 	 
 driver.findElement(By.id("p_method_postilion_creditcard")).click();   // selecting credit card radio button under payment method
 Select objSelect1 = new Select(driver.findElement(By.id("postilion_creditcard_cc_type")));
 objSelect1.selectByValue("MC");
 driver.findElement( By.id("postilion_creditcard_cc_number")).sendKeys("5413330089010475");
 driver.findElement(By.id("postilion_creditcard_cc_owner")).sendKeys("anu");
 Select objSelect2 = new Select(driver.findElement(By.id("postilion_creditcard_expiration")));
 objSelect2.selectByValue("12");
 Select objSelect3 = new Select(driver.findElement(By.id("postilion_creditcard_expiration_yr")));
 objSelect3.selectByValue("2025");	  
 driver.findElement(By.id("postilion_creditcard_cc_cid")).sendKeys("160");
 driver.findElement(By.id("onestepcheckout-place-order")).click();
 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
 driver.switchTo().frame("centinel_authenticate_iframe");
 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
 driver.findElement(By.name("external.field.password")).sendKeys("test123");
 driver.findElement(By.name("UsernamePasswordEntry")).click(); 
 driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
 String actual_title2 = driver.getTitle();   // verifying page title
 System.out.println("page title:" +actual_title2);
 Assert.assertTrue(actual_title2.contains("Edgars")); 
 Thread.sleep(2000);

 //screen shot capturing
 TakesScreenshot ts2 =(TakesScreenshot)driver;
 File source2= ts2.getScreenshotAs(OutputType.FILE);
 FileUtils.copyFile(source2, new File("./Orders/Successfulorder.png"));
 System.out.println("screen shot taken");
 driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
//screen shot capturing end
   }
   
@AfterTest
   public void Logout() throws InterruptedException{
	//driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	driver.findElement(By.linkText("LOG OUT")).click();
	String expected = "Edgars | latest fashion for women, men�s and kids, beauty & homeware";
	String actual = driver.getTitle();
	Assert.assertEquals(actual, expected);
	System.out.println(actual);
	driver.quit();
	}
}


