package testng;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class ForgotYourPassword {
	
	public static WebDriver driver;
	public String expected = null;
	public String actual = null;
	public static String baseUrl = "http://qa.edgars.co.za";
	static String username;

	@BeforeTest
	public static void LaunchBrowser() {
		//System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
		driver = new FirefoxDriver();
		// driver = new FirefoxDriver();
		driver.get(baseUrl);
		String expected = "Edgars | latest fashion for women, men�s and kids, beauty & homeware";
		String actual = driver.getTitle();
		Assert.assertEquals(actual, expected);
		System.out.println(actual);
		driver.manage().window().maximize();
		driver.findElement(By.linkText("LOG IN")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		String expected1 = "Customer Login";
		String actual1 = driver.getCurrentUrl();
		Assert.assertEquals(actual1, expected1);
		System.out.println(actual1);
	}

	@Test(priority = 0)
	public static void forgotPassword() {
		driver.findElement(By.xpath("//a[@href='http://qa.edgars.co.za/customer/account/forgotpassword/']")).click();
		String expected2 = "http://qa.edgars.co.za/customer/account/forgotpassword/";
		String actual2 = driver.getCurrentUrl();

		Assert.assertEquals(actual2, expected2);

		if (expected2.equals(actual2)) {
         ForgotYourPassword.validLogins(username);
		} else {
			System.out.println("Failed to placed under 'forgot password' section");
		}
	}

	@Test(priority = 1, dataProvider = "testdata")
	public static void validLogins(String username) {
		driver.findElement(By.id("email_address")).sendKeys(username);
		driver.findElement(By.id("//button[@type='submit']")).click();
		ForgotYourPassword.gmailLogin();
	}

	@DataProvider(name = "testdata")
	public Object[][] readExcel() throws BiffException, IOException {
		// TODO Auto-generated method stub
		File f = new File("C:/Users/InventUser/Desktop/datadriven3.xls");
		Workbook w = Workbook.getWorkbook(f);
		Sheet s = w.getSheet("Forgotyourpassword");
		int rows = s.getRows();
		int columns = s.getColumns();

		String inputData[][] = new String[rows][columns];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				Cell c = s.getCell(j, i);
				inputData[i][j] = c.getContents();
				System.out.println(inputData[i][j]);
			}
		}
		return inputData;
	}

	public static void gmailLogin() {
		String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
		driver.findElement(By.linkText("https://accounts.google.com/AddSession?sacu=1&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail#identifier")).sendKeys(selectLinkOpeninNewTab);
		driver.findElement(By.id("Email")).sendKeys("qatesting.commerce@gmail.com");
		driver.findElement(By.id("next")).click();
		String expected ="Sign in - Google Accounts";
		String actual= driver.getTitle();
		try{
			if(expected.equals(actual)){
				driver.findElement(By.id("Passwd")).sendKeys("qatestqa");
				driver.findElement(By.id("signIn")).click();
			}
			else{
				System.out.println("User not placed under 'Sign in - Google Accounts' section");
			}
			}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
	
		driver.findElement(By.id("Passwd")).sendKeys("qatestqa");
		driver.findElement(By.id("signIn")).click();
		boolean login = driver.findElement(By.xpath("//a[@href='https://mail.google.com/mail/u/0/#inbox']")).isDisplayed();
	  System.out.println("User logged into the gmail successfully" +login);
	}
}

	 
