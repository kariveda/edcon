package testng;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CreditTest {
	public static  WebDriver driver;
	public String expected = null;
	public String actual = null;
	
    public static String baseUrl = "http://qa.edgars.co.za";


    @BeforeTest
    public static void launchBrowser(){
	System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");
	driver = new ChromeDriver();
    	//driver = new FirefoxDriver();
	driver.get(baseUrl);
	String expected = "Edgars | latest fashion for women, men�s and kids, beauty & homeware";
	String actual = driver.getTitle();
	Assert.assertEquals(actual, expected);
	System.out.println(actual);
	driver.manage().window().maximize();
}

@Test(priority = 0)
public static void logIn() throws InterruptedException{
driver.findElement(By.linkText("LOG IN")).click();
String expected = "Customer Login";
String actual = driver.getTitle();
Thread.sleep(3000);
Assert.assertEquals(actual, expected);
System.out.println(actual);
}

@Test(priority=1,dataProvider = "testdata")
public void validLogins(String username,String password)
{
driver.findElement(By.name("login[username]")).sendKeys(username);
driver.findElement(By.name("login[password]")).sendKeys(password);
driver.findElement(By.id("send2")).click();
//driver.manage().timeouts().implicitlyWait(2,TimeUnit.MINUTES);
Assert.assertEquals("http://qa.edgars.co.za/customer/account", driver.getCurrentUrl());
}


@DataProvider(name = "testdata")
public Object[][] readExcel() throws BiffException, IOException {
	// TODO Auto-generated method stub
	File f = new File("C:/Users/InventUser/Desktop/datadriven3.xls");
	Workbook w = Workbook.getWorkbook(f);
	Sheet s = w.getSheet("Logins");
	int rows = s.getRows();
    int columns = s.getColumns();
    //System.out.println(rows);
    //System.out.println(columns);
    
    String inputData[][] = new String [rows][columns];
       for(int i=0; i<rows; i++) {
    	for(int j=0; j<columns; j++){
    		Cell c = s.getCell(j,i);
    		inputData[i][j] = c.getContents();
    		//System.out.println(inputData[i][j]);
    		}
    	}
   return inputData; 
}


public static void addToCart() throws InterruptedException, IOException{
	System.out.println(driver.getCurrentUrl());
	driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[3]/div/div/ul/li[4]/a/span[1]")).click();                    //Clicking on womens tab
	driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div[1]/div[2]/ul/li[1]/a")).click(); //clothing link
	//driver.findElement(By.xpath(".//html/body/div[2]/div/div/div[2]/div[2]/div/div[4]/div/div[2]/ul/li[1]/a/span")).click();  //clicking on dress---->Stage
	
	driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div/div[2]/ul/li[1]/a")).click();          //Clicking on the new arrivals sub tab
	driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[5]/div[2]/ul/li/h2/a")).click();                         //Clicking on the product "Format shirt SKU: 20077238" product
	driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[2]/div[2]/div/div[4]/div[2]/form/div[4]/div[4]/div/button")).click();  //Adding product to the cart
	Thread.sleep(3000);
	driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[1]/div/div/div[2]/div/div/div/div[8]/div/div/div/a/span/span[2]")).click();  //Click on the cart symbol
	driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
	driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[1]/div/div/div[2]/div/div/div/div[8]/div/div/div/a/span/span[2]")).click();  //Click on the cart symbol
	driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[1]/div/div/div[2]/div/div/div/div[8]/div/div/div/div/div[2]/button[2]")).click();      //click on the check out button under cart
	Assert.assertEquals(1, 1);
	System.out.println(driver.getCurrentUrl());
}


public static void address(){
	WebElement address = driver.findElement(By.id("shipping-address-select"));  //Selecting second option from drop down list under 'deliver to' section
	  Select objSelect = new Select(driver.findElement(By.id("shipping-address-select")));
	  objSelect.selectByIndex(1);
	  System.out.println("address: "+ driver.getCurrentUrl());
}


public static void shippingmethod(){
	List<WebElement> radio=driver.findElements(By.xpath("//html/body/div[1]/div/div/div[2]/div[2]/div/div[3]/form/fieldset/div/div[2]/div[1]/div/dl/dt[1]/input"));  //For selecting shipping method WOFS/Click and collect
	  for(int i=0;i<radio.size();i++)
	   {
	 	  WebElement local_radio=radio.get(i);
	  	  String value=local_radio.getAttribute("value");
	  	  System.out.println("Shipping method is" +value);
	 	  
	 	  if(value.equalsIgnoreCase("wofs_wofs"))  	  {
	 		  local_radio.click();
	 	  }
	   }
	  System.out.println("Shippingmethod: "+ driver.getCurrentUrl());
}

	
@Test(priority= 5,dataProvider = "testdata1") 
public static void creditCards(String cardtype,String cardnumber, String cardname, String cmonth,String cyear, String cvn, String validcard ) throws InterruptedException, IOException
{
	
	CreditTest.addToCart();
	boolean refresh = utility.ReUsable.refreshPage("One Step Checkout", driver, 15);
	
	CreditTest.address();
	CreditTest.shippingmethod();
	
	driver.findElement(By.id("p_method_postilion_creditcard")).click();   // selecting credit card radio button under payment method
	 Select objSelect1 = new Select(driver.findElement(By.id("postilion_creditcard_cc_type")));
	 objSelect1.selectByValue(cardtype);
	 driver.findElement( By.id("postilion_creditcard_cc_number")).sendKeys(cardnumber);
	 driver.findElement(By.id("postilion_creditcard_cc_owner")).sendKeys(cardname);
	 
	// WebElement address = driver.findElement(By.id("postilion_creditcard_expiration"));  //Selecting option from drop down list for country
	  Select objSelect = new Select(driver.findElement(By.id("postilion_creditcard_expiration"))); 
	  objSelect.selectByValue(cmonth);
	  
	
	 
	 //WebElement address1 = driver.findElement(By.id("postilion_creditcard_expiration_yr"));  //Selecting option from drop down list for country
	  Select objSelect2 = new Select(driver.findElement(By.id("postilion_creditcard_expiration_yr"))); 
	  objSelect2.selectByValue(cyear);
	 
	 driver.findElement(By.id("postilion_creditcard_cc_cid")).sendKeys(cvn);
	 driver.findElement(By.id("onestepcheckout-place-order")).click();
	 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 
	 driver.switchTo().frame("centinel_authenticate_iframe");
	 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 driver.findElement(By.name("external.field.password")).sendKeys("test123");
	 driver.findElement(By.name("UsernamePasswordEntry")).click(); 
	 driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
	 String actual_title2 = driver.getTitle();   // verifying page title
	 System.out.println("page title:" +actual_title2);
	 Assert.assertTrue(actual_title2.contains("Edgars")); 
	 Thread.sleep(2000);

}

@DataProvider(name = "testdata1")
public Object[][] readExcel1() throws BiffException, IOException {
	// TODO Auto-generated method stub
	File f = new File("C:/Users/InventUser/Desktop/datadriven3.xls");
	Workbook w = Workbook.getWorkbook(f);
	Sheet s = w.getSheet("CreditCard");
	int rows = s.getRows();
    int columns = s.getColumns();
    //System.out.println(rows);
    //System.out.println(columns);
    
    String inputData[][] = new String [rows][columns];
       for(int i=0; i<rows; i++) {
    	for(int j=0; j<columns; j++){
    		Cell c = s.getCell(j,i);
    		inputData[i][j] = c.getContents();
    		System.out.println(inputData[i][j]);
    		}
    	}
   return inputData; 
}
}