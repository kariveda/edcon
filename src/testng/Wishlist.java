package testng;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class Wishlist {
	
public static WebDriver driver;

@Test(priority= 0)
 public void login() throws IOException
	{
	System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");	
	driver = new ChromeDriver();
	//driver = new FirefoxDriver();
	driver.get("http://qa.edgars.co.za/");
	driver.manage().window().maximize();
	driver.findElement(By.linkText("LOG IN")).click();
	driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
	driver.findElement(By.name("login[username]")).sendKeys("qatesting.commerce@gmail.com");
	driver.findElement(By.name("login[password]")).sendKeys("edgars#");
	driver.findElement(By.id("send2")).click();
	driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
	Assert.assertEquals("http://qa.edgars.co.za/customer/account", driver.getCurrentUrl());
  }
@Test(priority =1)
public void addToCart() throws InterruptedException, IOException{
	
	driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[3]/div/div/ul/li[4]/a/span[1]")).click();                    //Clicking on womens tab
	 driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div[1]/div[2]/ul/li[1]/a")).click(); //clothing link
	driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div/div[2]/ul/li[1]/a")).click();          //Clicking on the new arrivals sub tab 
   driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[5]/div[2]/ul/li/h2/a")).click();                         //Clicking on the product "Format shirt SKU: 20077238" product	
driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[2]/div[2]/div/div[4]/div[2]/form/div[4]/div[7]/ul/li[1]/a")).click();  //Adding product to the wishlist
String actual_title = driver.getTitle();                           // verifying page title
System.out.println("page title:" +actual_title);
Assert.assertTrue(actual_title.contains("My Wishlist"));
driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
driver.findElement(By.xpath(".//html/body/div/div/div/div[1]/div/div/div[3]/div/div/ul/li[5]/a/span[1]")).click();                             //  select men's tab
driver.findElement(By.xpath(".//*[@id='root-wrapper']/div/div/div[2]/div[2]/div/div[4]/div[1]/div[2]/ul/li[1]/span")).click(); //clothing
driver.findElement(By.xpath(".//*[@id='root-wrapper']/div/div/div[2]/div[2]/div/div[4]/div[1]/div[2]/ul/li[1]/ul/li[1]/a/span")).click();//new arrivals  
driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[5]/div[2]/div[1]/ul/li[1]/div[1]")).click();                            //click on the product "43758348"
driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[2]/div[2]/div/div[4]/div[2]/form/div[4]/div[7]/ul/li[1]/a")).click();           //add product to wishlist
String actual_title1 = driver.getTitle();                           // verifying page title
System.out.println("page title:" +actual_title1);
Assert.assertTrue(actual_title1.contains("My Wishlist")); 
}

@Test(priority =2)
public void addOperation() {
	driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div/div[1]/form[1]/fieldset/table/tbody/tr[1]/td[3]/div/div[2]/input")).clear();
	driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div/div[1]/form[1]/fieldset/table/tbody/tr[1]/td[3]/div/div[2]/input")).sendKeys("3");
	driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div/div[1]/form[1]/fieldset/div/button[3]")).click();  //update shopping cart
	driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div/div[1]/form[1]/fieldset/table/tbody/tr[2]/td[4]/a")).click(); //click on the close button
	driver.switchTo().alert().accept();  //accept the alert
	driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div/div[1]/form[1]/fieldset/table/tbody/tr/td[4]/a")).click(); //click on the close button 2nd product
	driver.switchTo().alert().dismiss();  //cancel the alert
}

@Test(priority =3)
public void wishlistLinks(){
	driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[1]/div/div/div[1]/div/div/div[2]/div[2]/div/div/div/div/ul/li[3]/a")).click(); //click on wishlist link
	driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
	driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div/div[1]/form[1]/fieldset/table/tbody/tr/td[3]/div/p/a")).click(); //edit link under wishlist
	//driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[2]/div[2]/div/div[4]")).click(); //clicking on the current page
	//driver.findElement(By.xpath("String")).sendKeys(Keys.BACK_SPACE);
	driver.findElement(By.xpath(".//html/body/div[1]/div/div/div[1]/div/div/div[1]/div/div/div[2]/div[2]/div/div/div/div/ul/li[3]/a")).click(); //click on wishlist link
	driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
    driver.findElement(By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div/div[1]/form[1]/fieldset/table/tbody/tr/td[2]/h3/a")).click();  // clicking on product link
    //driver.findElement(By.xpath("String")).sendKeys(Keys.BACK_SPACE);
    String actual_title1 = driver.getTitle();                           // verifying page title
    System.out.println("page title:" +actual_title1);
     }   
	
@AfterTest
public void Logout() throws InterruptedException{
	//driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	driver.findElement(By.linkText("LOG OUT")).click();
	String expected = "Edgars | latest fashion for women, men�s and kids, beauty & homeware";
	String actual = driver.getTitle();
	Assert.assertEquals(actual, expected);
	System.out.println(actual);
	driver.quit();
	}
}



/*
-->logout user Womens catagory ->add product to the catagory click on wishlist and add them to cart cart should be updated
-->login women catagory -> add product to wishlist */