package testng;

import java.awt.Robot;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.thoughtworks.selenium.webdriven.commands.KeyEvent;

public class ThankyouCard {

	public static WebDriver driver;
	public String expected = null;
	public String actual = null;

	public static String baseUrl = "http://qa.edgars.co.za";

	@BeforeTest
	public static void launchBrowser() {
		System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(baseUrl);
		/*
		 * String expected =
		 * "Edgars | latest fashion for women, men�s and kids, beauty & homeware"
		 * ; String actual = driver.getTitle(); Assert.assertEquals(actual,
		 * expected); System.out.println(actual);
		 */
		driver.manage().window().maximize();
	}

	@Test(priority = 0)
	public void login() throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		// WebDriver driver = new FirefoxDriver();
		driver.get("http://qa.edgars.co.za/");
		driver.manage().window().maximize();
		driver.findElement(By.linkText("LOG IN")).click();

		driver.findElement(By.name("login[username]")).sendKeys(
				"qatesting.commerce@gmail.com");
		driver.findElement(By.name("login[password]")).sendKeys("edgars#");
		driver.findElement(By.id("send2")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		/*
		 * String actual_title = driver.getTitle(); // verifying page title
		 * System.out.println("page title:" +actual_title);
		 * Assert.assertTrue(actual_title.contains("My Account"));
		 */
		driver.findElement(
				By.xpath(".//html/body/div/div/div/div[1]/div/div/div[3]/div/div/ul/li[4]/a/span[1]"))
				.click(); // Clicking on womens tab
		driver.findElement(
				By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[4]/div[1]/div[2]/ul/li[1]/ul/li[1]/a"))
				.click(); // Clicking on the new arrivals sub tab
		driver.findElement(
				By.xpath(".//html/body/div/div/div/div[2]/div[2]/div/div[5]/div[2]/ul/li/h2/a"))
				.click(); // Clicking on the product
							// "Format shirt SKU: 20077238" product
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(
				By.xpath(".//html/body/div[1]/div/div/div[2]/div[2]/div/div[4]/div[2]/form/div[4]/div[4]/div/button"))
				.click(); // Adding product to the cart
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(
				By.xpath(".//html/body/div[1]/div/div/div[1]/div/div/div[2]/div/div/div/div[8]/div/div/div/a/span/span[2]"))
				.click(); // Click on the cart symbol
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(
				By.xpath(".//html/body/div[1]/div/div/div[1]/div/div/div[2]/div/div/div/div[8]/div/div/div/a/span/span[2]"))
				.click(); // Click on the cart symbol
		driver.findElement(
				By.xpath(".//html/body/div[1]/div/div/div[1]/div/div/div[2]/div/div/div/div[8]/div/div/div/div/div[2]/button[2]"))
				.click(); // click on the check out button under cart
		Thread.sleep(2000);
		/*
		 * String actual_title1 = driver.getTitle(); // verifying page title
		 * System.out.println("page title:" +actual_title1);
		 * Assert.assertTrue(actual_title1.contains("One Step Checkout"));
		 */// user placed in checkout section
		// WebElement address =
		// driver.findElement(By.id("shipping-address-select")); //Selecting
		// second option from drop down list under 'deliver to' section
		Select objSelect = new Select(driver.findElement(By
				.id("shipping-address-select")));
		objSelect.selectByIndex(1);

		List<WebElement> radio = driver
				.findElements(By
						.xpath("//html/body/div[1]/div/div/div[2]/div[2]/div/div[3]/form/fieldset/div/div[2]/div[1]/div/dl/dt[1]/input")); // For
																																			// selecting
																																			// shipping
																																			// method
																																			// WOFS/Click
																																			// and
																																			// collect
		for (int i = 0; i < radio.size(); i++) {
			WebElement local_radio = radio.get(i);
			String value = local_radio.getAttribute("value");
			System.out.println("Shipping method is" + value);

			if (value.equalsIgnoreCase("wofs_wofs")) {
				local_radio.click();
			}
		}
		driver.findElement(By.id("p_method_postilion_accountcard")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// driver.switchTo().window("postilion-link-account-form");
		driver.findElement(By.name("edcon_card_number")).sendKeys(
				"0006008158234526494");
		driver.findElement(By.name("mobile_number")).sendKeys("0738454870");
		driver.findElement(
				By.xpath(".//html/body/div[4]/div[2]/form/div[2]/button"))
				.click();

	}

	@Test(priority = 1)
	public void backEndLogin() {

		String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL, Keys.RETURN);
		driver.findElement(By.linkText("http://qa.edgars.co.za/admin"))
				.sendKeys(selectLinkOpeninNewTab);
		driver.findElement(
				By.xpath(".//html/body/div[1]/div[1]/div[3]/ul/li[11]/a/span"))
				.click();
		driver.findElement(
				By.xpath(".//html/body/div[1]/div[1]/div[3]/ul/li[11]/a/span"))
				.click();
		driver.findElement(
				By.xpath(".//html/body/div[1]/div[1]/div[3]/ul/li[11]/ul/li[2]/a/span"))
				.click();
		driver.navigate().refresh();
		driver.findElement(
				By.id("statusGrid_filter_date_sent1486979568.1932_from_trig"))
				.click();
		List<WebElement> allDates = driver.findElements(By
				.xpath("/html/body/div[4]"));
		for (WebElement ele : allDates) {

			String date = ele.getText();

			if (date.equalsIgnoreCase("13")) {
				ele.click();
				break;
			}
		}
		driver.findElement(By.id("id_25e93564a3dcf86291dd4243eaf3acc0"))
				.click();
		driver.findElement(
				By.id("/html/body/div[1]/div[5]/div/div[3]/div/div[2]/div/table/tbody/tr[2]/td[4]/a"))
				.click();
	}
}
