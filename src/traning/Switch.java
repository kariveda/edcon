package traning;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Switch {
  
	public static WebDriver driver;
	@Test(priority=1)
  public void browserLaunch() {
		System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");	
		driver = new ChromeDriver();  //Chrome driver
		driver.get("http://qa.edgars.co.za/");  //Url 
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);   
		driver.manage().window().maximize();  //maximising window
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		
		}
	@Test(priority=2)
	public void switchToAnother(){
		driver.findElement(By.cssSelector("Body")).sendKeys(Keys.CONTROL + "t");  //open empty tab
		driver.get("http://qa.edgars.co.za/admin");  //open new link in empty tab
		
	}
}
