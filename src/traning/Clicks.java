package traning;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Clicks {
	public static WebDriver driver;
	
@BeforeTest
	public static void launchBrowser(){
	System.setProperty("webdriver.chrome.driver","C:/chromedriver.exe");
	driver = new ChromeDriver();
     driver.get("https://197.96.20.230:9002/store/?site=clicks");
     driver.manage().window().maximize();
     driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
	String expected = "Welcome to Clicks - Your Pharmacy, Health, Home and Beauty Store";
    String actual = driver.getTitle();
    Assert.assertEquals(actual, expected);System.out.println(actual);
    }
     @Test(priority = 0)
	public static void registerBtn()
{
driver.findElement(By.xpath("//*[@id='page']/header/div[2]/div[1]/div/div/div[2]/ul/li[1]/a")).click();
	driver.findElement(By.xpath("//button[text()='Create account']")).click();
driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	String expected1 = "Welcome to Clicks - Your Pharmacy, Health, Home and Beauty Store";
String actual1 = driver.getTitle();
	Assert.assertEquals(actual1, expected1);
	System.out.println(actual1);
	}
@Test(priority = 1)
	public static void regeisterRequired()  {
		
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[text()='No, I would like to create an online account without a ClubCard']")).click();
}
}